﻿using Programming.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Programming.Entities
{
    public abstract class BaseEntity<T> where T : BaseEntity<T>, new()
    {
        private static string _filePath;

        public BaseEntity()
        {
            var directoryPath = Directory.GetCurrentDirectory() + "\\Data";
            Directory.CreateDirectory(directoryPath);
            var fileName = this.GetType().Name + ".txt";
            _filePath = directoryPath + "\\" + fileName;
        }

        #region Properties

        public string Id { get; set; }

        #endregion

        #region Functions

        public void Save()
        {
            if (string.IsNullOrWhiteSpace(Id))
            {
                Insert();
            }
            else
            {
                Update();
            }

        }

        public void Delete()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Id))
                {
                    throw new ArgumentNullException("The Id field is invalid.");
                }

                var data = File.ReadAllLines(_filePath);

                var newDataList = new List<string>();

                foreach (var item in data)
                {
                    if (!item.Split('|').Any(c => c == Id))
                    {
                        newDataList.Add(item);
                    }
                }

                var newData = newDataList.ToArray();

                File.WriteAllLines(_filePath, newData);

                // Reset object
                foreach (var property in GetType().GetProperties())
                {
                    property.SetValue(this, null);
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Errors occurred while deleting the entity: \n" + ex.Message);
            }

        }

        public static T Find(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }

                var data = File.ReadAllLines(_filePath);

                string[] foundItem = new string[] { };
                foreach (var item in data)
                {
                    var itemArray = item.Split('|');
                    if (itemArray.Any(c => c == id))
                    {
                        foundItem = itemArray;
                        break;
                    }
                }

                var entity = new T();

                // Populate entity properties
                var properties = GetEntityProperties(entity);

                for (int i = 0; i < properties.Count; i++)
                {
                    var property = properties[i];
                    property.SetValue(entity, foundItem[i]);
                }

                // Populate navigation properties
                var navigationProperties = GetNavigationProperties(entity);
                foreach (var navigationProperty in navigationProperties)
                {
                    var foreignKeyPropertyName = navigationProperty.PropertyType.Name + "Id";
                    var foreignKeyProperty = properties.SingleOrDefault(c => c.Name == foreignKeyPropertyName);
                    var foreignKeyValue = foreignKeyProperty.GetValue(entity);

                    // Fetch the navigation property
                    var navigationPropertyType = Type.GetType(navigationProperty.PropertyType.FullName);
                    var findMethod = navigationPropertyType.GetMethod(nameof(Find), BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
                    var navigationPropertyValue = findMethod.Invoke(null, new[] { foreignKeyValue });

                    navigationProperty.SetValue(entity, navigationPropertyValue);
                }

                return entity;
            }
            catch (Exception ex)
            {

                throw new Exception("Errors occurred while finding the entity: \n" + ex.Message);
            }
        }

        private void Insert()
        {
            try
            {
                Id = Guid.NewGuid().ToString();

                var entityProperties = GetEntityProperties(this);
                var navigationProperties = GetNavigationProperties(this);


                // Save the navigation properties and update the "ForeignKey" properties
                foreach (var navigationProperty in navigationProperties)
                {
                    // Save details of the navigation property
                    var navigationPropertyType = navigationProperty.PropertyType;
                    var saveMethod = navigationPropertyType.GetMethod(nameof(Save));
                    var navigationEntity = navigationProperty.GetValue(this);
                    saveMethod.Invoke(navigationEntity, null);

                    // Retrieve the saved navigation property Id so that you can update the "ForeignKey" property of this instance
                    var navigationEntityIdProperty = navigationEntity
                                                        .GetType()
                                                        .GetProperties()
                                                        .SingleOrDefault(c => c.Name == "Id");

                    var navigationEntityId = string.Empty;
                    if (navigationEntityIdProperty != null)
                    {
                        navigationEntityId = navigationEntityIdProperty.GetValue(navigationEntity).ToString();
                    }

                    // Update the "Foreign Key" property (The assumption is that the foreign key will be the name of the navigation property + "Id")
                    var foreignKeyPropertyName = navigationPropertyType.Name + "Id";
                    var foreignKeyProperty = entityProperties.SingleOrDefault(c => c.Name == foreignKeyPropertyName);

                    if (foreignKeyProperty != null)
                    {
                        foreignKeyProperty.SetValue(this, navigationEntityId);
                    }

                }

                // Then save the other properties
                var recordRow = string.Empty;
                foreach (var property in entityProperties)
                {
                    recordRow += property.GetValue(this);

                    if (property != entityProperties.Last())
                    {
                        recordRow += "|";
                    }
                }

                using (StreamWriter file = new StreamWriter(_filePath, true))
                {
                    file.WriteLine(recordRow);
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Errors occurred while inserting data: \n" + ex.Message);
            }
        }

        private void Update()
        {
            // Logic to update would come here. This is not required for this implementation

        }

        private static List<PropertyInfo> GetEntityProperties(BaseEntity<T> entity)
        {
            var properties = entity.GetType()
                                    .GetProperties()
                                    .Where(c => !c.PropertyType.IsSubClassOfGeneric(typeof(BaseEntity<>)))
                                    .OrderBy(c => c.Name)
                                    .ToList();

            return properties;
        }

        private static List<PropertyInfo> GetNavigationProperties(BaseEntity<T> entity)
        {
            var properties = entity.GetType()
                                    .GetProperties()
                                    .Where(c => c.PropertyType.IsSubClassOfGeneric(typeof(BaseEntity<>)))
                                    .OrderBy(c => c.Name)
                                    .ToList();

            return properties;
        }

        #endregion

        #region Overrides

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !GetType().Equals(obj.GetType()))
            {
                return false;
            }

            var properties = GetType().GetProperties();

            foreach (var property in properties)
            {
                var instanceValue = property.GetValue(this);
                var comparerValue = obj.GetType().GetProperty(property.Name).GetValue(obj);

                if (!instanceValue.Equals(comparerValue))
                {
                    return false;
                }
            }

            return true;

        }

        #endregion

    }
}
