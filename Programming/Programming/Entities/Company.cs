﻿namespace Programming.Entities
{
    public class Company : BaseEntity<Company>
    {
        public Company()
        {

        }

        public Company(string name, Address address)
        {
            Name = name;
            Address = address;
        }

        // This is a foreign key. Convention for this app is to add "Id" to the navigation property name. 
        //Navigation property == Address, Foreign Key property = AddressId
        public string AddressId { get; set; }
        public string Name { get; set; }
        public Address Address { get; set; }
    }
}
