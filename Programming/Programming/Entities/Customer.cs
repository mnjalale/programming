﻿namespace Programming.Entities
{
    public class Customer : BaseEntity<Customer>
    {
        public Customer()
        {

        }

        public Customer(string firstName, string lastName, Address address)
        {
            FirstName = firstName;
            LastName = lastName;
            Address = address;
        }

        // This is a foreign key. Convention for this app is to add "Id" to the navigation property name. 
        //Navigation property == Address, Foreign Key property = AddressId
        public string AddressId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Address Address { get; set; }
    }
}
